package uk.co.paulbenn.regression;

import Jama.Matrix;
import Jama.QRDecomposition;
import com.google.common.math.Stats;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Multiple linear regression attempts to estimate the relationship between two or more independent (predictor)
 * variables {@code {X1, X2, ..., Xn}}, and one dependent (predicted) variable {@code Y}.
 *
 * <p>A multiple linear regression is usually expressed as:
 *
 * <pre>
 *     Y = β0 + β1*X1 + β2*X2 + ... + βn*Xn + E
 * </pre>
 *
 * <p>The set of coefficients {@code {β0, β1, ..., βn}} is collectively referred to as just {@code β} ("beta").
 * {@code E} is the model error. MLR attempts to compute a set of coefficients {@code β} such that the multiplication of
 * {@code β} and the set of independent variables {@code X} results in the straight line that best fits the observed
 * samples (results in the least residual sum of squares).
 */
@SuppressWarnings("UnstableApiUsage")
public class MultipleLinearRegression {

    /**
     * Threshold value. If matrix size is larger than {@code QR_DECOMPOSITION_THRESHOLD}, the coefficients matrix Beta
     * will be computed with the classical algorithm instead of QR decomposition.
     *
     * @see Mode
     */
    private static final int QR_DECOMPOSITION_THRESHOLD = 500;

    /**
     * Default rounding accuracy.
     *
     * @see MultipleLinearRegression#roundingAccuracy
     */
    private static final int DEFAULT_ROUNDING_ACCURACY = 5;

    /**
     * Rounding accuracy: how many decimal places to use when returning predicted variables, and when displaying stats.
     *
     * <p>{@code double} values are rounded using {@link BigDecimal}. This avoids floating point arithmetic errors.
     */
    private final int roundingAccuracy;

    /**
     * Independent predictor variables (one set per row).
     */
    private final Matrix x;
    /**
     * Dependent predicted variable (single row, one value per column).
     */
    private final Matrix y;

    /**
     * Solution matrix, which hold regression coefficients.
     */
    private Matrix coefficients;

    /**
     * Coefficient computation strategy.
     *
     * @see MultipleLinearRegression#QR_DECOMPOSITION_THRESHOLD
     */
    private final Mode mode;

    private double residualSumOfSquares;

    private double totalSumOfSquares;

    private double rSquared;

    private Stats moreStats;

    public MultipleLinearRegression(double[][] x, double[] y) {
        this(x, y, DEFAULT_ROUNDING_ACCURACY);
    }

    public MultipleLinearRegression(double[][] x, double[] y, int roundingAccuracy) {
        if (x.length != y.length) {
            throw new IllegalArgumentException("Number of rows in X must be equal to number of columns in Y.");
        }

        if (roundingAccuracy < 0) {
            throw new IllegalArgumentException("roundingAccuracy must be >= 0");
        }

        this.roundingAccuracy = roundingAccuracy;

        this.mode = x.length > QR_DECOMPOSITION_THRESHOLD
                ? Mode.CLASSIC
                : Mode.QR_DECOMPOSITION;

        this.x = new Matrix(x);
        this.y = new Matrix(y, y.length);

        this.computeCoefficients();
    }

    public double predict(double[] x) {
        double[] coefficients = this.coefficients.getRowPackedCopy();

        if (x.length != coefficients.length) {
            throw new RuntimeException("Number of coefficients must equal to number of predictor variables");
        }

        double predicted = 0;

        for (int i = 0; i < coefficients.length; i++) {
            predicted += x[i] * coefficients[i];
        }

        return round(predicted);
    }

    private void computeCoefficients() {
        coefficients = mode.computeCoefficients(x, y);

        computeStats();
    }

    // TODO (BENN-36): put all MLR stats into their own object
    private void computeStats() {
        /* Compute total sum of squares */
        double[] y = this.y.getColumnPackedCopy();
        totalSumOfSquares = round(totalSumOfSquares(y));

        /* Compute residual sum of squares */
        Matrix residuals = x.times(coefficients).minus(this.y);
        residualSumOfSquares = round(residuals.norm2() * residuals.norm2());

        /* Compute R squared */
        rSquared = round(1 - (residualSumOfSquares / totalSumOfSquares));

        /* Compute other statistics */
        moreStats = Stats.of(y);
    }

    private double totalSumOfSquares(double... values) {
        return totalSumOfSquares(Stats.meanOf(values), values);
    }

    private double totalSumOfSquares(double mean, double... values) {
        double sumOfSquares = 0;
        for (double value : values) {
            double deviation = value - mean;
            sumOfSquares += deviation * deviation;
        }
        return sumOfSquares;
    }

    private double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(roundingAccuracy, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Multiple Linear Regression - Results\n\n")
                .append("RSS (Residual Sum of Squares)      = ")
                .append(residualSumOfSquares).append("\n")
                .append("TSS (Total Sum of Squares)         = ")
                .append(totalSumOfSquares).append("\n")
                .append("R^2 (Coefficient of determination) = ")
                .append(rSquared)
                .append(" (").append(round(rSquared * 100)).append("% accuracy)\n")
                .append("Y Mean                             = ")
                .append(round(moreStats.mean())).append("\n")
                .append("Y Variance                         = ")
                .append(round(moreStats.populationVariance())).append("\n")
                .append("Y Standard Deviation               = ")
                .append(round(moreStats.populationStandardDeviation())).append("\n")
                .append("\nBeta Coefficients:\n[");

        double[] betaCoefficients = coefficients.getRowPackedCopy();

        for (int i = 0; i < betaCoefficients.length; i++) {
            sb.append("B").append(i).append(" = ").append(round(betaCoefficients[i])).append(", ");
        }

        sb.delete(sb.length() - 2, sb.length()).append("]\n\n");
        sb.append("In equation form:\nY ~= ");
        for (int i = 0; i < betaCoefficients.length; i++) {
            double coefficient = round(betaCoefficients[i]);
            sb.append(coefficient < 0 ? "" : "+").append(coefficient);
            if (i == 0) {
                sb.append(" ");
            } else {
                sb.append(" X").append(i).append(" ");
            }
        }

        return sb.deleteCharAt(sb.length() - 1).toString();
    }

    private enum Mode {
        CLASSIC {
            @Override
            protected Matrix computeCoefficients(Matrix predictors, Matrix predicted) {
                Matrix transpose = predictors.transpose();
                return transpose.times(predicted).times((transpose.times(predictors)).inverse());
            }
        },
        QR_DECOMPOSITION {
            @Override
            protected Matrix computeCoefficients(Matrix predictors, Matrix predicted) {
                QRDecomposition qr = new QRDecomposition(predictors);
                return qr.solve(predicted);
            }
        };

        protected abstract Matrix computeCoefficients(Matrix predictors, Matrix predicted);
    }
}

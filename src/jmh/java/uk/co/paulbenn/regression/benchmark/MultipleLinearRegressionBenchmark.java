package uk.co.paulbenn.regression.benchmark;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.infra.Blackhole;
import uk.co.paulbenn.regression.MultipleLinearRegression;

@SuppressWarnings("unused")
public class MultipleLinearRegressionBenchmark {

    @Benchmark
    @BenchmarkMode(Mode.All)
    public void benchmark(Blackhole blackHole, BenchmarkState benchmarkState) {
        MultipleLinearRegression mlr = new MultipleLinearRegression(
                benchmarkState.independentVariables,
                benchmarkState.dependentVariable
        );
        blackHole.consume(mlr);
    }

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        public double[][] independentVariables = {
                {1, 1, 2, 3},
                {1, 3, 2, 1},
                {1, 1, 0, 0},
                {1, 100, 300, 200},
        };
        public double[] dependentVariable = {3, 11, 4, 703};
    }
}

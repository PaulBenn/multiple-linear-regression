package uk.co.paulbenn.regression;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MultipleLinearRegressionTest {

    @Test
    void qrDecompositionOk() {
        double[][] independentVariables = {
                {1, 1, 2, 3},
                {1, 3, 2, 1},
                {1, 1, 0, 0},
                {1, 100, 300, 200},
        };
        double[] dependentVariable = {3, 11, 4, 703};

        MultipleLinearRegression mlr = new MultipleLinearRegression(independentVariables, dependentVariable);

        assertEquals(6, mlr.predict(new double[]{2, 2, 4, 6}));
    }
}

# Multiple Linear Regression
Multiple linear regression demo, using NIST's [JAMA](https://math.nist.gov/javanumerics/jama/).

This sample project uses two of JAMA's algorithms, and picks between them based on the size of the independent variable matrix `X`.
- If `X` is large, use classic regression: matrix transpose and inverse operations.
- If `X` is below a set threshold, use QR decomposition.

### Example usage
```java
double[][] independentVariables = {
        {1, 1, 2, 3},
        {1, 3, 2, 1},
        {1, 1, 0, 0},
        {1, 100, 300, 200},
};
double[] dependentVariable = {3, 11, 4, 703};

MultipleLinearRegression mlr = new MultipleLinearRegression(independentVariables, dependentVariable);

double predictedValue = mlr.predict(new double[]{2, 2, 4, 6})
```

To view statistics, print the result of `MultipleLinearRegression::toString`:
```text
Multiple Linear Regression - Results

RSS (Residual Sum of Squares)      = 0.0
TSS (Total Sum of Squares)         = 364394.75
R^2 (Coefficient of determination) = 1.0 (100.0% accuracy)
Y Mean                             = 180.25
Y Variance                         = 91098.6875
Y Standard Deviation               = 301.82559

Beta Coefficients:
[B0 = 3.0, B1 = 1.0, B2 = 4.0, B3 = -3.0]

In equation form:
Y ~= +3.0 +1.0 X1 +4.0 X2 -3.0 X3
```

# Performance
TODO ([BENN-35](https://paulbenn.atlassian.net/browse/BENN-35)): benchmark MLR using [JMH](https://github.com/openjdk/jmh).
